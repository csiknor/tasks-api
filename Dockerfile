FROM gradle:jdk11 as build
WORKDIR /app
COPY build.gradle .
COPY settings.gradle .
COPY src src
RUN gradle build
RUN java -Djarmode=layertools -jar build/libs/api.jar extract

FROM openjdk:11-jre-slim
WORKDIR /app
COPY --from=build /app/dependencies/ ./
COPY --from=build /app/spring-boot-loader/ ./
COPY --from=build /app/snapshot-dependencies/ ./
COPY --from=build /app/application/ ./
EXPOSE 8080
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
