# Tasks API

Component managing users and tasks through an exposed REST API. Data is stored in underlying DB.

## Execution

The application can be executed as a standalone Java application after being built using Gradle or as a Docker 
container.

### Gradle

Requires JDK 11

```shell
./gradlew bootRun
```

### Docker

Requires Docker runtime.

```shell
docker build -t csiknor/tasks-api . && docker run -it --rm -p 8080:8080 csiknor/tasks-api
```

## Usage

### Create user
```shell
curl -i -H "Content-Type: application/json" -X POST \
  -d '{"username": "jsmith", "first_name": "John", "last_name": "Smith"}' \
  http://localhost:8080/api/user
```

### Update user
```shell
curl -i -H "Content-Type: application/json" -X PUT \
  -d '{"id": <id>, "username": "jsmith", "first_name": "John", "last_name": "Doe"}' \
  http://localhost:8080/api/user/{id}
```

### List all users
```shell
curl -i -H "Content-Type: application/json" -X GET http://localhost:8080/api/user
```

### Get User info
```shell
curl -i -H "Content-Type: application/json" -X GET http://localhost:8080/api/user/{id}
```
Expecting this structure (for the User):
```json
{ 
  "id": 1,
  "username": "jsmith",
  "first_name": "James",
  "last_name": "Smith"
}
```

### Create Task
```shell
curl -i -H "Content-Type: application/json" -X POST \
  -d '{"name": "My task", "description": "Description of task", "date_time": "2016-05-25 14:25:00Z"}' \
  http://localhost:8080/api/user/{user_id}/task
```

### Update Task
```shell
curl -i -H "Content-Type: application/json" -X PUT \
  -d '{"id":1, "name": "My updated task" ,"date_time": "2016-05-25 14:25:00Z"}' \
  http://localhost:8080/api/user/{user_id}/task/{task_id}
```

### Delete Task
```shell
curl -i -X DELETE http://localhost:8080/api/user/{user_id}/task/{task_id}
```

### Get Task Info
```shell
curl -i -H "Content-Type: application/json" -X GET \
  http://localhost:8080/api/user/{user_id}/task/{task_id}
```

### List all tasks for a user

```shell
curl -i -H "Content-Type: application/json" -X GET \
  http://localhost:8080/api/user/{user_id}/task
```

## Implementation

The component uses Spring Boot, Spring MVC and Spring Data JPA with in H2 database instance. Endpoints defined in the
following controllers:

* [`UserController`](src/main/java/csiknor/tasksapi/user/UserController.java)
* [`TaskController`](src/main/java/csiknor/tasksapi/task/TaskController.java)

Periodic task closure is a [`TaskService`](src/main/java/csiknor/tasksapi/task/TaskService.java) feature.

Database change management is implemented using Liquibase in the [`db.changelog`](src/main/resources/db/changelog/db.changelog-master.yaml).

Build and deployment is done using Docker images as per the [`Dockerfile`](Dockerfile). 

### TODO
* Implement security: authentication and authorization
* Introduce `PROD` profile with managed DB
* On top of existing E2E test add integration test for repositories and unit tests for services
* Split DB changelog into multiple files
