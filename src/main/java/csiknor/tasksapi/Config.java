package csiknor.tasksapi;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * Application configuration for Spring context
 */
@Configuration
@EnableScheduling
public class Config {
    @PostConstruct
    public void setUp() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}
