package csiknor.tasksapi.task;

import csiknor.tasksapi.user.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * Represents a task entity in the database.
 */
@Data
@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ssX")
    private ZonedDateTime dateTime;
    @ManyToOne
    private User user;
    @Enumerated(EnumType.STRING)
    private State state = State.PENDING;

    public enum State { PENDING, DONE }
}
