package csiknor.tasksapi.task;

import csiknor.tasksapi.task.dto.NewTaskDto;
import csiknor.tasksapi.task.dto.TaskRequestDto;
import csiknor.tasksapi.task.dto.TaskResponseDto;
import csiknor.tasksapi.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * REST API endpoint for {@link Task} management on "/api/user/{userId}/task" endpoint.
 */
@RestController
@RequestMapping("/api/user/{userId}/task")
@RequiredArgsConstructor
@Slf4j
public class TaskController {
    private final TaskService service;
    private final UserService userService;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping
    public List<TaskResponseDto> getTasks(@PathVariable Long userId) {
        log.info("Get tasks");
        return service.listAll(userId).stream().map(this::toTaskDto).collect(toList());
    }

    @GetMapping("/{taskId}")
    public TaskResponseDto getTask(@PathVariable Long userId, @PathVariable Long taskId) {
        log.info("Get task: #{}/#{}", userId, taskId);
        return toTaskDto(service.get(taskId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TaskResponseDto addTask(@PathVariable Long userId, @Valid @RequestBody NewTaskDto newTaskDto) {
        log.info("Add task #{}: {}", userId, newTaskDto);
        Task task = toTask(newTaskDto);
        task.setUser(userService.get(userId));
        return toTaskDto(service.add(task));
    }

    @PutMapping("/{taskId}")
    public TaskResponseDto modifyTask(
            @PathVariable Long userId,
            @PathVariable Long taskId,
            @Valid @RequestBody TaskRequestDto taskRequestDto
    ) {
        log.info("Modify task #{}/#{}: {}", userId, taskId, taskRequestDto);
        if (!taskId.equals(taskRequestDto.getId())) {
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "Incorrect id");
        }
        Task task = toTask(taskRequestDto);
        task.setUser(userService.get(userId));
        return toTaskDto(service.modify(task));
    }

    @DeleteMapping("/{taskId}")
    public void removeTask(@PathVariable Long userId, @PathVariable Long taskId) {
        log.info("Remove task #{}/#{}", userId, taskId);
        service.remove(taskId);
    }

    private TaskResponseDto toTaskDto(Task task) {
        return modelMapper.map(task, TaskResponseDto.class);
    }

    private Task toTask(NewTaskDto dto) {
        return modelMapper.map(dto, Task.class);
    }

    private Task toTask(TaskRequestDto dto) {
        return modelMapper.map(dto, Task.class);
    }
}
