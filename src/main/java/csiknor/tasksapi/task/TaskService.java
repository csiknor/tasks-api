package csiknor.tasksapi.task;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class TaskService {
    private final TaskRepository repository;

    public List<Task> listAll(Long userId) {
        return repository.findAllByUserId(userId);
    }

    public Task add(Task task) {
        return repository.save(task);
    }

    public Task modify(Task aTask) {
        Task storedTask = repository.getById(aTask.getId());
        BeanUtils.copyProperties(aTask, storedTask);
        return storedTask;
    }

    public void remove(Long taskId) {
        if (repository.existsById(taskId)) {
            repository.deleteById(taskId);
        }
    }

    public Task get(Long taskId) {
        return repository.findById(taskId).orElseThrow(EntityNotFoundException::new);
    }

    /**
     * Scheduled periodic execution of task closure. Every 10 seconds, tasks in pending state and past date time are
     * moved to done state and logged to console.
     */
    @Scheduled(fixedDelay = 10, timeUnit = TimeUnit.SECONDS)
    public void closeTasks() {
        log.info("Close tasks");
        List<Task> expiredTasks = repository.findAllByStateAndDateTimeBefore(Task.State.PENDING, ZonedDateTime.now());
        expiredTasks.forEach(TaskService::recordCompletion);
    }

    private static void recordCompletion(Task task) {
        System.out.println("Task completed: " + task);
        task.setState(Task.State.DONE);
    }
}
