package csiknor.tasksapi.task.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Data
public class TaskRequestDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;
    private String description;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ssX")
    @NotNull
    private ZonedDateTime dateTime;
}
