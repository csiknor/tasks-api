package csiknor.tasksapi.user;

import csiknor.tasksapi.user.dto.NewUserDto;
import csiknor.tasksapi.user.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * REST API endpoint for {@link User} management on "/api/user" endpoint.
 */
@RestController
@RequestMapping(path = "/api/user")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final UserService service;
    private final ModelMapper modelMapper = new ModelMapper();

    @GetMapping
    public List<UserDto> getUsers() {
        log.info("Get users");
        return service.listAll().stream().map(this::toUserDto).collect(toList());
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable Long id) {
        log.info("Get user: #{}", id);
        return toUserDto(service.get(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto addUser(@Valid @RequestBody NewUserDto newUserDto) {
        log.info("Add user: {}", newUserDto);
        return toUserDto(service.add(toUser(newUserDto)));
    }

    @PutMapping("/{id}")
    public UserDto modifyUser(@PathVariable Long id, @Valid @RequestBody UserDto userDto) {
        log.info("Modify user #{}: {}", id, userDto);
        if (!id.equals(userDto.getId())) {
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "Incorrect id");
        }
        return toUserDto(service.modify(toUser(userDto)));
    }

    private UserDto toUserDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    private User toUser(NewUserDto dto) {
        return modelMapper.map(dto, User.class);
    }

    private User toUser(UserDto dto) {
        return modelMapper.map(dto, User.class);
    }
}
