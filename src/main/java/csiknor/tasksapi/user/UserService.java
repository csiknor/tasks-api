package csiknor.tasksapi.user;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class UserService {
    private final UserRepository repository;

    public List<User> listAll() {
        return repository.findAll();
    }

    public User add(User user) {
        if (repository.existsByUsername(user.getUsername())) {
            throw new EntityExistsException();
        }
        return repository.save(user);
    }

    public User modify(User aUser) {
        User storedUser = repository.getById(aUser.getId());
        BeanUtils.copyProperties(aUser, storedUser);
        return storedUser;
    }

    public User get(Long id) {
        return repository.findById(id).orElseThrow(EntityNotFoundException::new);
    }
}
