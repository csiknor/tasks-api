package csiknor.tasksapi.user.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class NewUserDto {
    @NotBlank
    @Size(max = 10)
    private String username;
    @Size(max = 100)
    private String firstName;
    @Size(max = 100)
    private String lastName;
}
