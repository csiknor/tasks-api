package csiknor.tasksapi.task;

import csiknor.tasksapi.user.User;
import csiknor.tasksapi.user.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TaskControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    TaskRepository repository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    ObjectMapper objectMapper;

    private User user;

    @BeforeEach
    void setUp() throws Exception {
        user = createUser();
    }

    @AfterEach
    void tearDown() {
        repository.deleteAll();
        userRepository.delete(user);
    }

    @Test
    void getsEmptyTasks() throws Exception {
        mockMvc.perform(get("/api/user/{userId}/task", user.getId()))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.length()", equalTo(0))
                );
    }

    @Test
    void getsTasks() throws Exception {
        Task task = new Task();
        task.setUser(user);
        task.setName("name");
        task.setDescription("Short desc");
        task.setDateTime(ZonedDateTime.of(2022, 2, 22, 10, 15, 55, 333, ZoneId.of("CET")));
        repository.save(task);

        mockMvc.perform(get("/api/user/{userId}/task", user.getId()))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$[0].name", equalTo("name")),
                        jsonPath("$[0].description", equalTo("Short desc")),
                        jsonPath("$[0].user_username", equalTo("test1")),
                        jsonPath("$[0].date_time", equalTo("2022-02-22 09:15:55Z")),
                        jsonPath("$[0].state", equalTo("PENDING")),
                        jsonPath("$.length()", equalTo(1))
                );
    }

    @Test
    void addsNewTask() throws Exception {
        mockMvc.perform(post("/api/user/{userId}/task", user.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"Some\", \"description\": \"Interesting\", \"date_time\": \"2022-01-05 14:10:55Z\"}"))
                .andExpectAll(
                        status().isCreated(),
                        jsonPath("$.id", notNullValue()),
                        jsonPath("$.name", equalTo("Some")),
                        jsonPath("$.description", equalTo("Interesting")),
                        jsonPath("$.state", equalTo("PENDING"))
                );
    }

    @Test
    void doesNotAddNewTaskForNonExistingUser() throws Exception {
        mockMvc.perform(post("/api/user/{userId}/task", user.getId() + 99)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"Some\", \"description\": \"Interesting\", \"date_time\": \"2022-01-05 14:10:55Z\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    void doesNotAddNewTaskWithoutName() throws Exception {
        mockMvc.perform(post("/api/user/{userId}/task", user.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"description\": \"Interesting\", \"date_time\": \"2022-01-05 14:10:55Z\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void modifiesExistingTask() throws Exception {
        Task task = createTask();

        mockMvc.perform(put("/api/user/{userId}/task/{taskId}", user.getId(), task.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":" + task.getId() + ", \"name\": \"Better\", \"date_time\": \"2022-01-05 14:10:55Z\"}"))
                .andExpectAll(
                        status().isOk(),
                        MockMvcResultMatchers.jsonPath("$.id", equalTo(task.getId().intValue())),
                        jsonPath("$.name", equalTo("Better")),
                        jsonPath("$.user_username", equalTo(user.getUsername())),
                        jsonPath("$.state", equalTo("PENDING"))
                );
    }

    @Test
    void doesNotModifyNonExistingTask() throws Exception {
        mockMvc.perform(put("/api/user/{userId}/task/{taskId}", user.getId(), 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":" + 1 + ", \"name\": \"Better\", \"date_time\": \"2022-01-05 14:10:55Z\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    void deletesExistingTask() throws Exception {
        mockMvc.perform(delete("/api/user/{userId}/task/{taskId}", user.getId(), createTask().getId()))
                .andExpect(status().isOk());
    }

    @Test
    void deletesNonExistingTask() throws Exception {
        mockMvc.perform(delete("/api/user/{userId}/task/{taskId}", user.getId(), 1))
                .andExpect(status().isOk());
    }

    @Test
    void getsExistingTask() throws Exception {
        Task task = createTask();

        mockMvc.perform(get("/api/user/{userId}/task/{taskId}", user.getId(), task.getId()))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.name", equalTo("Some")),
                        jsonPath("$.description", equalTo("Interesting")),
                        jsonPath("$.date_time", equalTo("2022-01-05 14:10:55Z"))
                );
    }

    private User createUser() throws Exception {
        String response = mockMvc.perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"username\": \"test1\"}"))
                .andReturn().getResponse().getContentAsString();

        return objectMapper.readValue(response, User.class);
    }

    private Task createTask() throws Exception {
        String response = mockMvc.perform(post("/api/user/{userId}/task", user.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"Some\", \"description\": \"Interesting\", \"date_time\": \"2022-01-05 14:10:55Z\"}"))
                .andReturn().getResponse().getContentAsString();

        return objectMapper.readValue(response, Task.class);
    }
}