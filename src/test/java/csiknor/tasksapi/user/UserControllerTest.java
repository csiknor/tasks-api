package csiknor.tasksapi.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository repository;

    @AfterEach
    void tearDown() {
        repository.deleteAll();
    }

    @Test
    void getsEmptyUsers() throws Exception {
        mockMvc.perform(get("/api/user")).andExpectAll(
                status().isOk(),
                jsonPath("$.length()", equalTo(0)));
    }

    @Test
    void addsNewUser() throws Exception {
        mockMvc.perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"username\": \"test1\"}"))
                .andExpectAll(
                        status().isCreated(),
                        jsonPath("$.id", notNullValue()));
    }

    @Test
    void doesNotAddNewUserWithLongUsername() throws Exception {
        mockMvc.perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"username\": \"test1verylong\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void doesNotAddExistingUser() throws Exception {
        createUser();

        mockMvc.perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"username\": \"test1\"}"))
                .andExpect(status().isConflict());
    }

    @Test
    void modifiesExistingUser() throws Exception {
        User user = createUser();

        mockMvc.perform(put("/api/user/{id}", user.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"id\": " + user.getId() + ", \"username\": \"myuser\", \"first_name\": \"Some\" }"))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.id", equalTo(user.getId().intValue())),
                        jsonPath("$.username", equalTo("myuser")),
                        jsonPath("$.first_name", equalTo("Some")),
                        jsonPath("$.last_name", nullValue())
                );
    }

    @Test
    void doesNotModifyNonExistingUser() throws Exception {
        mockMvc.perform(put("/api/user/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"id\": 1, \"username\": \"myuser\", \"first_name\": \"Some\", \"last_name\": \"Folk\" }"))
                .andExpect(status().isNotFound());
    }

    @Test
    void getsExistingUser() throws Exception {
        User user = createUser();

        mockMvc.perform(get("/api/user/{id}", user.getId()))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.username", equalTo("test1"))
                );
    }

    @Test
    void doesNotGetNonExistingUser() throws Exception {
        mockMvc.perform(get("/api/user/{id}", 1))
                .andExpect(status().isNotFound());
    }

    private User createUser() throws Exception {
        String response = mockMvc.perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"username\": \"test1\"}"))
                .andReturn().getResponse().getContentAsString();

        return objectMapper.readValue(response, User.class);
    }
}